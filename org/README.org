* Welcome To My Personal Website
  Hello, and welcome to my personal website! This is a work in progress, so any input would be greatly appreciated.

  To view the website, follow the link here: https://www.alrocad.com
* File Organization
** src
   Contains the source code files. Currently a work in progress.
*** css
	CSS directory. Will contain the CSS for various web pages.
*** images
	Originally just a directory for images I plan on hosting on my website, but storing all the photos on one server is a bit much.

	I shall leave the files already there alone for the time being, but long term goal is to host files in seperate server, and then feed those files to the website.
** org
   I am using emacs to maintain this website, and as such I will be using org files to implement a lot of things.

   If you are unfamiliar with emacs or org-mode, I recommend taking a look at these links.

   [[https://www.gnu.org/software/emacs/manual/html_node/emacs/index.html][Emacs Manual]]

   [[https://orgmode.org/manual/][Org-Mode Manual]]
