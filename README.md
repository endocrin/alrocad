
# Table of Contents

1.  [Welcome To My Personal Website](#orgd69ac73)
2.  [File Organization](#org7f85d01)
    1.  [src](#orgebd76e4)
        1.  [css](#orgdc3afbf)
        2.  [images](#org94e3762)
    2.  [org](#org1a63d93)


<a id="orgd69ac73"></a>

# Welcome To My Personal Website

Hello, and welcome to my personal website! This is a work in progress, so any input would be greatly appreciated.

To view the website, follow the link here: <https://www.alrocad.com>


<a id="org7f85d01"></a>

# File Organization


<a id="orgebd76e4"></a>

## src

Contains the source code files. Currently a work in progress.


<a id="orgdc3afbf"></a>

### css

CSS directory. Will contain the CSS for various web pages.


<a id="org94e3762"></a>

### images

Originally just a directory for images I plan on hosting on my website, but storing all the photos on one server is a bit much.

I shall leave the files already there alone for the time being, but long term goal is to host files in seperate server, and then feed those files to the website.


<a id="org1a63d93"></a>

## org

I am using emacs to maintain this website, and as such I will be using org files to implement a lot of things.

If you are unfamiliar with emacs or org-mode, I recommend taking a look at these links.

[Emacs Manual](https://www.gnu.org/software/emacs/manual/html_node/emacs/index.html)

[Org-Mode Manual](https://orgmode.org/manual/)

